// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDEOXAJFpH-WwEkul99PoLUpmQmZ4xM62k",
    authDomain: "exam-angular-4bb69.firebaseapp.com",
    projectId: "exam-angular-4bb69",
    storageBucket: "exam-angular-4bb69.appspot.com",
    messagingSenderId: "131402082776",
    appId: "1:131402082776:web:14a61f74e4a32b1bc52aad"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
