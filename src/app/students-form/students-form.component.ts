import { StudentsService } from './../students.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'studentsForm',
  templateUrl: './students-form.component.html',
  styleUrls: ['./students-form.component.css']
})
export class StudentsFormComponent implements OnInit {

  tuitions:Object[] = [{id:1,name:'true'},{id:2,name:'false'}]
  psycho:number
  math:number
  tuition:boolean

  loading: boolean
  fallsOut: boolean
  result: any

  constructor(private studentService: StudentsService) { }
  @Output() closeEdit = new EventEmitter<null>()

  ngOnInit(): void {
  }
  tellParentToClose(){
    this.closeEdit.emit();
  }

  askPredict() {
    this.loading = true;
    this.studentService.predict(this.math, this.psycho, this.tuition).subscribe((res:any) => {
      console.log(res);
      this.result = res;
      this.fallsOut = res.result <= 0.5;
      this.loading = false;
    })
  }

  cancel() {
    this.fallsOut = null;
    this.math = 0;
    this.psycho = 0;
    this.tuition = false;
    this.result = null;
  }

}
