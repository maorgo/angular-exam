import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { StudentsFormComponent } from './students-form/students-form.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  /*{ path: 'temperatures/:city', component: TemperaturesComponent },*/
  { path: 'welcome', component: WelcomeComponent},
  { path: 'login', component: LoginComponent},
  { path: 'signup', component: SignUpComponent }, 
  { path: 'students', component: StudentsFormComponent }, 
   
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }