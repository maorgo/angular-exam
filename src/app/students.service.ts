import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  lamda: string = 'https://osq8nu16va.execute-api.us-east-1.amazonaws.com/beta';

  constructor(private http:HttpClient) {
    
   }


   predict(math, psycho, tuition) {
      return this.http.post(this.lamda, JSON.stringify({math, psycho, tuition}));
   }
}
